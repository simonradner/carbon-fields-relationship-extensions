<?php

namespace Carbon_Fields\Field;
use Sef\CarbonFieldsRelationshipExtensions\AbstractRelationField;

class Many_Post_Foreign_Relation_Field extends AbstractRelationField {

  protected $supportedContexts = [
    'Term_Meta',
    'Post_Meta'
  ];

  protected function syncDeletePostIds($postId, array $idsToBeDeleted, array $newIds, array $oldIds )
  {
    foreach($idsToBeDeleted as $id )
    {
      delete_post_meta( $id, $this->name );
    }
  }

  protected function syncSavePostIds($postId, array $idsToBeSaved, array $newIds, array $oldIds )
  {
    foreach($idsToBeSaved as $id )
    {
      update_post_meta( $id, $this->name, $postId, true );
    }
  }

  protected function findAttachedPostIds( $postId )
  {

    $q = new \WP_Query([
      'fields' => 'ids',
      'posts_per_page' => -1,
      'post_type' => $this->post_type,
      'post_status' => 'all',
      'meta_query' => [
        [
          'key'     => $this->name,
          'value'   => $postId,
          'compare' => '=',
        ]
      ]
    ]);

    return $q->posts;
  }

}
