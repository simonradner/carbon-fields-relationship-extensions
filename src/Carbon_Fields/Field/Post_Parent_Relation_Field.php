<?php

namespace Carbon_Fields\Field;
use Sef\CarbonFieldsRelationshipExtensions\AbstractRelationField;

class Post_Parent_Relation_Field extends AbstractRelationField {

  protected $max = 1;

  public function set_max( $max )
  {
    // do noting, mx MUST be 1
		return $this;
	}

  protected function syncDeletePostIds($postId, array $idsToBeDeleted, array $newIds, array $oldIds )
  {
    if( !empty( $idsToBeDeleted ) )
    {
      wp_update_post(
        [
          'ID' => $postId,
          'post_parent' => 0
        ]
      );
    }
  }

  protected function syncSavePostIds($postId, array $idsToBeSaved, array $newIds, array $oldIds )
  {
    if( !empty( $idsToBeSaved ) )
    {
      wp_update_post(
        [
          'ID' => $postId,
          'post_parent' => $idsToBeSaved[0]
        ]
      );
    }
  }

  protected function findAttachedPostIds( $postId )
  {
    $post = get_post($postId);
    return ( $post->post_parent ) ? [$post->post_parent] : [];
  }

}
