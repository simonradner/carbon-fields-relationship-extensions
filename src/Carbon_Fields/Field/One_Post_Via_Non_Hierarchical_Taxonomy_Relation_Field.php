<?php

namespace Carbon_Fields\Field;
use Sef\CarbonFieldsRelationshipExtensions\AbstractRelationField;

class One_Post_Via_Non_Hierarchical_Taxonomy_Relation_Field extends Many_Post_Via_Non_Hierarchical_Taxonomy_Relation_Field {

  protected $max = 1;

  public function set_max( $max )
  {
    // do not allow to set a max value, max MUST be 1
		return $this;
	}

}
