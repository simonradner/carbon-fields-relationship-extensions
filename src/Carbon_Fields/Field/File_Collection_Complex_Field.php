<?php

namespace Carbon_Fields\Field;
use Carbon_Fields\Field\Complex_Field;

class File_Collection_Complex_Field extends Complex_Field {

  protected $fileCollectionConfigs;

  protected $validFields = [
    'File',
    'Image'
  ];

  public function get_type()
  {
    $class = get_parent_class( $this ); // setting the type to be the same as the complex field
		return $this->clean_type( 'Carbon_Fields\\Field\\Complex_Field' );
	}

  // public API

  public function setFileCollectionConfigs( array $configs )
  {
    $defaults = [
      'targetFieldName'       => null,
      'destinationFieldName'  => null,
      'saveStrategy' => 'unserialized'
    ];
    $parsedConfigs = [];

    foreach($configs as $config )
    {
      if ( is_array( $config ) && isset( $config['targetFieldName'] ) )
      {
        $parsedConfig = wp_parse_args( $config, $defaults );
        if( ! $parsedConfig['destinationFieldName'])
        {
          $parsedConfig['destinationFieldName'] = $parsedConfig['targetFieldName'];
        }
        $parsedConfigs[] = $parsedConfig;
      }
    }
    $this->fileCollectionConfigs = $parsedConfigs;
    return $this;
  }

  // overwrite saving
  public function save()
  {
    parent::save();

    $postId = ($this->get_datastore()->get_id()) ? $this->get_datastore()->get_id() : false;

    if( false === $postId )
    {
      return;
    }

    foreach( $this->fileCollectionConfigs as $config )
    {
      if( ! $this->isConfigValid( $config ))
      {
        continue;
      }

      $values = $this->getAggregatedFileCollectionAttachmentIds( $config['targetFieldName'] );
      $keyName = $config['destinationFieldName'];

      // save attachment ids as readable data
      switch( $config['saveStrategy'] )
      {
        case 'serialized':
          delete_post_meta( $postId, $keyName );
          update_post_meta( $postId, $keyName, $values );
        break;

        case 'unserialized':
        default:
          delete_post_meta( $postId, $keyName );
          foreach( $values as $value )
          {
            if( $value )
            {
              add_post_meta( $postId, $keyName, $value );
            }
          }
        break;
      }
    }

  }

  protected function getAggregatedFileCollectionAttachmentIds( $targetFieldBaseName )
  {
    $values = [];
    foreach ( $this->values as $value ) {
      foreach ( $value as $field ) {
        if($field->get_base_name() === $targetFieldBaseName )
        {
          $values[] = $field->get_value();
        }
      }
    }
    return $values;
  }

  protected function isConfigValid( $config )
  {

    if( ! is_array( $config ) )
      return false;

    $validFieldFound = false;
    foreach ($this->get_fields() as $field )
    {
      if( in_array( $field->get_type(), $this->validFields ) )
      {
        $validFieldFound = true;
      }
    }
    return $validFieldFound;
  }
}
