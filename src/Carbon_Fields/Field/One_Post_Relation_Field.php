<?php

namespace Carbon_Fields\Field;

class One_Post_Relation_Field extends Many_Post_Relation_Field {

  protected $max = 1;

  public function set_max( $max )
  {
    // do noting, mx MUST be 1
		return $this;
	}

}
