<?php

namespace Carbon_Fields\Field;
use Sef\CarbonFieldsRelationshipExtensions\AbstractRelationField;

class Many_Post_Via_Non_Hierarchical_Taxonomy_Relation_Field extends AbstractRelationField {

  protected $supportedContexts = [
    'Post_Meta'
  ];

  protected function findAttachedPostIds($postId)
  {
    switch( $this->get_context())
    {
      case 'Post_Meta';
        $terms = wp_get_post_terms( $postId, $this->get_name(), [
          'fields' => 'names'
        ]);
        return (is_array( $terms )) ? $terms : [];
      break;
    }
  }

  protected function syncDeletePostIds($postId, array $idsToBeDeleted, array $newIds, array $oldIds )
  {
    // syncSavePostIds does all the work
    // return;
  }

  protected function syncSavePostIds($postId, array $idsToBeSaved, array $newIds, array $oldIds )
  {

    switch( $this->get_context())
    {
      case 'Post_Meta';

        // ids must be array of strings not ints // ['101', '32', ...]
        $allIds = array_merge( $newIds, $oldIds );
        if( ! empty( $allIds ))
        {
          wp_set_post_terms( $postId, $newIds, $this->get_name(), false );
        } else {
          wp_delete_object_term_relationships( $postId, $this->get_name() );
        }

      break;
    }
  }
}
