<?php

namespace Carbon_Fields\Field;
use Sef\CarbonFieldsRelationshipExtensions\AbstractRelationField;

class Many_Post_Relation_Field extends AbstractRelationField {

  protected $supportedContexts = [
    'Term_Meta',
    'Post_Meta'
  ];

  protected function findAttachedPostIds($postId)
  {
    switch( $this->get_context())
    {
      case 'Post_Meta';
        return get_post_meta($postId, $this->name, false ); // yes, 3rd arg is false!
      break;
      case 'Term_Meta';
        return get_term_meta($postId, $this->name, false ); // yes, 3rd arg is false!
      break;
    }
  }

  protected function syncDeletePostIds($postId, array $idsToBeDeleted, array $newIds, array $oldIds )
  {
    foreach($idsToBeDeleted as $id )
    {
      switch( $this->get_context())
      {
        case 'Post_Meta';
          delete_post_meta($postId, $this->get_name(), $id );
        break;
        case 'Term_Meta';
          delete_term_meta($postId, $this->get_name(), $id );
        break;
      }
    }
  }

  protected function syncSavePostIds($postId, array $idsToBeSaved, array $newIds, array $oldIds )
  {
    foreach($idsToBeSaved as $id )
    {
      switch( $this->get_context())
      {
        case 'Post_Meta';
          add_post_meta($postId, $this->get_name(), $id );
        break;
        case 'Term_Meta';
          add_term_meta($postId, $this->get_name(), $id );
        break;
      }
    }
  }






}
