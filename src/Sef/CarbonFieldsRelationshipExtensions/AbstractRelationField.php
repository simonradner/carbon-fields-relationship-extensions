<?php

namespace Sef\CarbonFieldsRelationshipExtensions;
use Carbon_Fields\Field\Relationship_Field;

abstract class AbstractRelationField extends Relationship_Field {

  protected $supportedContexts = [
    'Post_Meta'
  ];

  public function get_type()
  {
    $class = get_parent_class( $this ); // setting the type to be the same as the map field
		return $this->clean_type( 'Carbon_Fields\\Field\\Relationship_Field' );
	}

  public function getPostId()
  {
    if( ! $this->isContextSupported() )
      return;
      
    return ($this->get_datastore()->get_id()) ? $this->get_datastore()->get_id() : false;
  }


    /**
  	 * Delegate load to the field DataStore instance
  	 **/
  	public function load()
    {
      if( ! $this->isContextSupported() )
        return;

      $postId = $this->getPostId();

      if( ! $postId )
      {
        return false;
      }

      $ids = $this->findAttachedPostIds( $postId );

  		if ( $ids === false ) {
  			$this->set_value( $this->default_value );
  		}

      $this->set_value( $ids );

  	}
  	/**
  	 * Delegate save to the field DataStore instance
  	 **/
  	public function save()
    {
      if( ! $this->isContextSupported() )
        return;

      $postId = $this->getPostId();
      if( ! $postId )
      {
        //@TODO throw exception
        return false;
      }
      $oldIds = $this->findAttachedPostIds($postId);

      if( false === $oldIds )
      {
        //@TODO throw exception
        return false;
      }

      $newIds = $this->get_value();
      if( ! is_array( $newIds ))
      {
        $newIds = [];
      }

      if( $newIds == $oldIds )
      {
        return; // nothing new
      }

       $idsToBeSaved = array_diff($newIds, $oldIds);
       $idsToBeDeleted = array_diff($oldIds, $newIds);

      // work
      $this->syncDeletePostIds($postId, $idsToBeDeleted, $newIds, $oldIds );
      $this->syncSavePostIds($postId, $idsToBeSaved, $newIds, $oldIds );
  	}
  	/**
  	 * Delegate delete to the field DataStore instance
  	 **/
  	public function delete()
    {
      if( ! $this->isContextSupported() )
        return;

      // do we need this ???
  	}

    protected function isContextSupported()
    {
      return in_array($this->get_context(), $this->supportedContexts );
    }

    abstract protected function findAttachedPostIds($postId);
    abstract protected function syncDeletePostIds($postId, array $idsToBeDeleted, array $newIds, array $oldIds );
    abstract protected function syncSavePostIds($postId, array $idsToBeSaved, array $newIds, array $oldIds );


}
